
let http = require("http");

http.createServer(function(request, response){
	
		
	if (request.url == "/" && request.method == "POST" ){
		response.writeHead(200, {'Content-Type': 'text/Plain'});
		response.end('Welcome to booking System');
	}

	if (request.url == "/profile" && request.method == "POST" ){
		response.writeHead(200, {'Content-Type': 'text/Plain'});
		response.end('Welcome to your profile');
	}

	if (request.url == "/courses" && request.method == "POST" ){
		response.writeHead(200, {'Content-Type': 'text/Plain'});
		response.end("Here's our courses");
	}

	if (request.url == "/addcourses" && request.method == "GET" ){
		response.writeHead(200, {'Content-Type': 'text/Plain'});
		response.end("Add a course to our resources");
	}

	if (request.url == "/updatecourse" && request.method == "PUT" ){
		response.writeHead(200, {'Content-Type': 'text/Plain'});
		response.end('Update courses to our resources');
	}

		if (request.url == "/archivecourses" && request.method == "DELETE" ){
		response.writeHead(200, {'Content-Type': 'text/Plain'});
		response.end('Archieve courses to our resources');
	}

}).listen(4000);

console.log('Server is running at localhost : 4000')